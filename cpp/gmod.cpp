#define GMMODULE

#include "Interface.h"
#include <stdio.h>

using namespace GarrysMod::Lua;

extern "C" {
	void 		glua_arg_error( lua_State* state, int args, const char* message ) { LUA->ArgError( args, message ); }
	void 		glua_call( lua_State* state, int args, int results ) { LUA->Call( args, results ); }
	double		glua_check_number( lua_State* state, int stack_pos ) { return LUA->CheckNumber( stack_pos ); }
	const char* glua_check_string( lua_State* state, int stack_pos ) { return LUA->CheckString( stack_pos ); }
	void 		glua_check_type( lua_State* state, int stack_pos, int type ) { LUA->CheckType( stack_pos, type ); }
	void 		glua_create_metatable_type( lua_State* state, const char* name, int type ) { LUA->CreateMetaTableType( name, type ); }
	void 		glua_create_table( lua_State* state ) { LUA->CreateTable( ); }
	int 		glua_equal( lua_State* state, int a, int b ) { return LUA->Equal( a, b );}
	bool 		glua_get_bool( lua_State* state, int stack_pos ) { return LUA->GetBool( stack_pos ); }
	CFunc 		glua_get_cfunction( lua_State* state, int stack_pos ) { return LUA->GetCFunction( stack_pos ); }
	void 		glua_get_field( lua_State* state, int stack_pos, const char* name ) { return LUA->GetField( stack_pos, name ); }
	bool 		glua_get_metatable( lua_State* state, int i ) { return LUA->GetMetaTable( i ); }
	double 		glua_get_number( lua_State* state, int stack_pos ) { return LUA->GetNumber( stack_pos ); }
	const char*	glua_get_string( lua_State* state, int stack_pos ) { return LUA->GetString( stack_pos ); }
	void 		glua_get_table( lua_State* state, int stack_pos ) { return LUA->GetTable( stack_pos ); }
	int 		glua_get_type( lua_State* state, int stack_pos ) { return LUA->GetType( stack_pos ); }
	const char*	glua_get_typename( lua_State* state, int stack_pos ) { return LUA->GetTypeName( stack_pos ); }
	void* 		glua_get_userdata( lua_State* state, int stack_pos ) { return LUA->GetUserdata( stack_pos ); }
	void 		glua_insert( lua_State* state, int stack_pos ) { LUA->Insert( stack_pos ); }
	bool 		glua_is_type( lua_State* state, int stack_pos, int type ) { return LUA->IsType( stack_pos, type ); }
	void* 		glua_new_userdata( lua_State* state, unsigned int size ) { return LUA->NewUserdata( size ); }
	int 		glua_next( lua_State* state, int stack_pos ) { return LUA->Next( stack_pos ); }
	int 		glua_pcall( lua_State* state, int args, int results, int error_func ) { return LUA->PCall( args, results, error_func ); }
	void 		glua_pop( lua_State* state, int amt ) { LUA->Pop( amt ); }
	void 		glua_push( lua_State* state, int stack_pos ) { LUA->Push( stack_pos ); }
	void 		glua_push_bool( lua_State* state, bool val ) { LUA->PushBool( val ); }
	void 		glua_push_cclosure( lua_State* state, CFunc val, int vars ) { LUA->PushCClosure( val, vars ); }
	void 		glua_push_cfunction( lua_State* state, CFunc val ) { LUA->PushCFunction( val ); }
	void 		glua_push_global( lua_State* state ) { LUA->PushSpecial( GarrysMod::Lua::SPECIAL_GLOB ); }
	void 		glua_push_nil( lua_State* state ) { LUA->PushNil( ); }
	void 		glua_push_number( lua_State* state, double val ) { LUA->PushNumber( val ); }
	void 		glua_push_special( lua_State* state, int type ) { LUA->PushSpecial( type ); }
	void 		glua_push_string( lua_State* state, const char* val ) { LUA->PushString( val ); }
	void 		glua_push_userdata( lua_State* state, void* val ) { LUA->PushUserdata( val ); }
	int 		glua_raw_equal( lua_State* state, int a, int b ) { return LUA->RawEqual( a, b ); }
	void 		glua_raw_get( lua_State* state, int stack_pos ) { LUA->RawGet( stack_pos ); }
	void 		glua_raw_set( lua_State* state, int stack_pos ) { LUA->RawSet( stack_pos ); }
	int 		glua_reference_create( lua_State* state ) { return LUA->ReferenceCreate( ); }
	void 		glua_reference_free( lua_State* state, int i ) { LUA->ReferenceFree( i ); }
	void 		glua_reference_push( lua_State* state, int i ) { LUA->ReferencePush( i ); }
	void 		glua_remove( lua_State* state, int stack_pos ) { LUA->Remove( stack_pos ); }
	void 		glua_set_field( lua_State* state, int stack_pos, const char* name ) { LUA->SetField( stack_pos, name ); }
	void 		glua_set_metatable( lua_State* state, int i ) { LUA->SetMetaTable( i ); }
	void 		glua_set_table( lua_State* state, int i ) { LUA->SetTable( i ); }
	void 		glua_throw_error( lua_State* state, const char* error ) { LUA->ThrowError( error ); }
	int 		glua_top( lua_State* state ) { return LUA->Top( ); }
}