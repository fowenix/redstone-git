# redstone-git
A simple git clone and fetch, reset library for garry's mod servers
## to build
* windows - build.bat
* linux - build.sh

these scripts automatically output the dll in the main folder gmsv_git_linux.dll or gmsv_git_win32.dll
## functions
* git.clone( url: string, path: string ) -> string
* git.fetch( path: string ) -> string
* git.reset( path: string ) -> string

path is starts inside the garrysmod folder, where the addon and gamemode folders are found.
reset is the same as ***git reset --hard origin/master***
## credits
wyozi for the gmod module based in rust-lang https://github.com/wyozi-gmod/gmod-rust-test