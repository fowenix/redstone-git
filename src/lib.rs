extern crate git2;
use std::ffi::{ CStr, CString };
use std::path::{ PathBuf };
pub type LuaState = *mut std::ffi::c_void;

extern {
	fn glua_arg_error( state: LuaState, args: i32, message: *const i8 );
	fn glua_call( state: LuaState, args: i32, results: i32 );
	fn glua_check_number( state: LuaState, stack_pos: i32 ) -> f64;
	fn glua_check_string( state: LuaState, stack_pos: i32 ) -> *const i8;
	fn glua_check_type( state: LuaState, stack_pos: i32, typ: i32 );
	fn glua_create_metatable_type( state: LuaState, name: *const i8, typ: i32 );
	fn glua_create_table( state: LuaState );
	fn glua_equal( state: LuaState, a: i32, b: i32 ) -> i32;
	fn glua_get_bool( state: LuaState, stack_pos: i32 ) -> bool;
	fn glua_get_cfunction( state: LuaState, stack_pos: i32 ) -> extern fn( LuaState ) -> i32;
	fn glua_get_field( state: LuaState, stack_pos: i32, name: *const i8 );
	fn glua_get_metatable( state: LuaState, i: i32 ) -> bool;
	fn glua_get_number( state: LuaState, stack_pos: i32 ) -> f64;
	fn glua_get_string( state: LuaState, stack_pos: i32 ) -> *const i8;
	fn glua_get_table( state: LuaState, stack_pos: i32 );
	fn glua_get_type( state: LuaState, stack_pos: i32 ) -> i32;
	fn glua_get_typename( state: LuaState, stack_pos: i32 ) -> *const i8;
	fn glua_get_userdata( state: LuaState, stack_pos: i32 ) -> LuaState;
	fn glua_insert( state: LuaState, stack_pos: i32 );
	fn glua_is_type( state: LuaState, stack_pos: i32, typ: i32 ) -> bool;
	fn glua_new_userdata( state: LuaState, size: u32 ) -> LuaState;
	fn glua_next( state: LuaState, stack_pos: i32 ) -> i32;
	fn glua_pcall( state: LuaState, args: i32, results: i32, error_func: i32 ) -> i32;
	fn glua_pop( state: LuaState, amt: i32 );
	fn glua_push( state: LuaState, stack_pos: i32 );
	fn glua_push_bool( state: LuaState, val: bool );
	fn glua_push_cclosure( state: LuaState, val: extern fn( LuaState ) -> i32, vars: i32 );
	fn glua_push_cfunction( state: LuaState, val: extern fn( LuaState ) -> i32 );
	fn glua_push_global( state: LuaState );
	fn glua_push_nil( state: LuaState );
	fn glua_push_number( state: LuaState, val: f64 );
	fn glua_push_special( state: LuaState, typ: i32 );
	fn glua_push_string( state: LuaState, val: *const i8 );
	fn glua_push_userdata( state: LuaState, val: LuaState );
	fn glua_raw_equal( state: LuaState, a: i32, b: i32 ) -> i32;
	fn glua_raw_get( state: LuaState, stack_pos: i32 );
	fn glua_raw_set( state: LuaState, stack_pos: i32 );
	fn glua_reference_create( state: LuaState ) -> i32;
	fn glua_reference_free( state: LuaState, i: i32 );
	fn glua_reference_push( state: LuaState, i: i32 );
	fn glua_remove( state: LuaState, stack_pos: i32 );
	fn glua_set_field( state: LuaState, stack_pos: i32, name: *const i8 );
	fn glua_set_metatable( state: LuaState, i: i32 );
	fn glua_set_table( state: LuaState, i: i32 );
	fn glua_throw_error( state: LuaState, error: *const i8 );
	fn glua_top( state: LuaState ) -> i32;
}

pub struct GLuaWrapper {
    state: LuaState
}

impl GLuaWrapper {
    pub fn arg_error( &self, args: i32, message: &str ) {
        unsafe { glua_arg_error( self.state, args, CString::new( message ).unwrap( ).as_ptr( ) ) }
    }

	pub fn call( &self, args: i32, results: i32 ) {
        unsafe { glua_call( self.state, args, results ) }
    }

	pub fn check_number( &self, stack_pos: i32 ) -> f64 {
        unsafe { glua_check_number( self.state, stack_pos ) }
    }

	pub fn check_string( &self, stack_pos: i32 ) -> String {
        unsafe { CStr::from_ptr( glua_check_string( self.state, stack_pos ) ).to_string_lossy( ).into_owned( ) }
    }
	pub fn check_type( &self, stack_pos: i32, typ: i32 ) {
        unsafe { glua_check_type( self.state, stack_pos, typ ) }
    }
	pub fn create_metatable_type( &self, name: &str, typ: i32 ) {
        unsafe { glua_create_metatable_type( self.state, CString::new( name ).unwrap( ).as_ptr( ), typ ) }
    }
	pub fn create_table( &self ) {
        unsafe { glua_create_table( self.state ) }
    }
	pub fn equal( &self, a: i32, b: i32 ) -> i32 {
        unsafe { glua_equal( self.state, a, b ) }
    }
	pub fn get_bool( &self, stack_pos: i32 ) -> bool {
        unsafe { glua_get_bool( self.state, stack_pos ) }
    }
	pub fn get_cfunction( &self, stack_pos: i32 ) -> extern fn( LuaState ) -> i32 {
        unsafe { glua_get_cfunction( self.state, stack_pos ) }
    }
	pub fn get_field( &self, stack_pos: i32, name: &str ) {
        unsafe { glua_get_field( self.state, stack_pos, CString::new( name ).unwrap( ).as_ptr( ) ) }
    }
	pub fn get_metatable( &self, i: i32 ) -> bool {
        unsafe { glua_get_metatable( self.state, i ) }
    }
	pub fn get_number( &self, stack_pos: i32 ) -> f64 {
        unsafe { glua_get_number( self.state, stack_pos ) }
    }
	pub fn get_string( &self, stack_pos: i32 ) -> String {
        unsafe { CStr::from_ptr( glua_get_string( self.state, stack_pos ) ).to_string_lossy( ).into_owned( ) }
    }
	pub fn get_table( &self, stack_pos: i32 ) {
        unsafe { glua_get_table( self.state, stack_pos ) }
    }
	pub fn get_type( &self, stack_pos: i32 ) -> i32 {
        unsafe { glua_get_type( self.state, stack_pos ) }
    }
	pub fn get_typename( &self, stack_pos: i32 ) -> String {
        unsafe { CStr::from_ptr( glua_get_typename( self.state, stack_pos ) ).to_string_lossy( ).into_owned( ) }
    }
	pub fn get_userdata( &self, stack_pos: i32 ) -> LuaState {
        unsafe { glua_get_userdata( self.state, stack_pos ) }
    }
	pub fn insert( &self, stack_pos: i32 ) {
        unsafe { glua_insert( self.state, stack_pos ) }
    }
	pub fn is_type( &self, stack_pos: i32, typ: i32 ) -> bool {
        unsafe { glua_is_type( self.state, stack_pos, typ ) }
    }
	pub fn new_userdata( &self, size: u32 ) -> LuaState {
        unsafe { glua_new_userdata( self.state, size ) }
    }
	pub fn next( &self, stack_pos: i32 ) -> i32 {
        unsafe { glua_next( self.state, stack_pos ) }
    }
	pub fn pcall( &self, args: i32, results: i32, error_func: i32 ) -> i32 {
        unsafe { glua_pcall( self.state, args, results, error_func ) }
    }
	pub fn pop( &self, amt: i32 ) {
        unsafe { glua_pop( self.state, amt ) }
    }
	pub fn push( &self, stack_pos: i32 ) {
        unsafe { glua_push( self.state, stack_pos ) }
    }
	pub fn push_bool( &self, val: bool ) {
        unsafe { glua_push_bool( self.state, val ) }
    }
	pub fn push_cclosure( &self, val: extern fn( LuaState ) -> i32, vars: i32 ) {
        unsafe { glua_push_cclosure( self.state, val, vars ) }
    }
	pub fn push_cfunction( &self, val: extern fn( LuaState ) -> i32 ) {
        unsafe { glua_push_cfunction( self.state, val ) }
    }
	pub fn push_global( &self ) {
        unsafe { glua_push_global( self.state ) }
    }
	pub fn push_nil( &self ) {
        unsafe { glua_push_nil( self.state ) }
    }
	pub fn push_number( &self, val: f64 ) {
        unsafe { glua_push_number( self.state, val ) }
    }
	pub fn push_special( &self, typ: i32 ) {
        unsafe { glua_push_special( self.state, typ ) }
    }
	pub fn push_string( &self, val: &str ) {
        unsafe { glua_push_string( self.state, CString::new( val ).unwrap( ).as_ptr( ) ); }
    }
	pub fn push_userdata( &self, val: LuaState ) {
        unsafe { glua_push_userdata( self.state, val ) }
    }
	pub fn raw_equal( &self, a: i32, b: i32 ) -> i32 {
        unsafe { glua_raw_equal( self.state, a, b ) }
    }
	pub fn raw_get( &self, stack_pos: i32 ) {
        unsafe { glua_raw_get( self.state, stack_pos ) }
    }
	pub fn raw_set( &self, stack_pos: i32 ) {
        unsafe { glua_raw_set( self.state, stack_pos ) }
    }
	pub fn reference_create( &self ) -> i32 {
        unsafe { glua_reference_create( self.state ) }
    }
	pub fn reference_free( &self, i: i32 ) {
        unsafe { glua_reference_free( self.state, i ) }
    }
	pub fn reference_push( &self, i: i32 ) {
        unsafe { glua_reference_push( self.state, i ) }
    }
	pub fn remove( &self, stack_pos: i32 ) {
        unsafe { glua_remove( self.state, stack_pos ) }
    }
	pub fn set_field( &self, stack_pos: i32, name: &str ) {
        unsafe { glua_set_field( self.state, stack_pos, CString::new( name ).unwrap( ).as_ptr( ) ) }
    }

	pub fn set_metatable( &self, i: i32 ) {
        unsafe { glua_set_metatable( self.state, i ) }
    }

	pub fn set_table( &self, i: i32 ) {
        unsafe { glua_set_table( self.state, i ) }
    }

	pub fn throw_error( &self, error: &str ) {
        unsafe { glua_throw_error( self.state, CString::new( error ).unwrap( ).as_ptr( ) ) }
    }

	pub fn top( &self  ) -> i32 {
        unsafe { glua_top( self.state ) }
    }

    pub fn set_global( &self, key: &str, func: extern fn( LuaState ) -> i32 ) {
        unsafe {
            glua_push_global( self.state );
            self.push_string( key );
            glua_push_cfunction( self.state, func );
            glua_set_table( self.state, -3 );
        }
    }
}

static mut LUA: Option< GLuaWrapper > = None;

extern fn clone( _: LuaState ) -> i32 {
    let wrapper = unsafe { LUA.as_ref( ).unwrap( ) };
    wrapper.check_string( 1 ); wrapper.check_string( 2 );
    let url = wrapper.get_string( 1 );
    let dir = wrapper.get_string( 2 );
    match git2::Repository::clone( &url, PathBuf::from( "garrysmod/" ).join( PathBuf::from( dir ) ) ) {
        Ok( _ ) => {
            wrapper.push_string( "successfully cloned repository" );
        },
        Err( err ) => {
            let err = "failed to clone repository \n".to_owned( ) + err.message( );
            wrapper.push_string( &err );
        }
    }
    1
}

extern fn fetch( _: LuaState ) -> i32 {
    let wrapper = unsafe { LUA.as_ref( ).unwrap( ) };
    wrapper.check_string( 1 );
    let dir = &wrapper.get_string( 1 );
    match git2::Repository::discover( PathBuf::from( "garrysmod/" ).join( PathBuf::from( dir ) ) ) {
        Ok( repo ) => {
            let mut remote = repo.find_remote( "origin" ).unwrap( );
            remote.fetch( &[ "master" ], None, None ).unwrap( );
            wrapper.push_string( "successfully fetched repository" );
        },
        Err( _ ) => {
            let dir = String::from( dir );
            let err = "failed to discover repository \n".to_owned( ) + &dir;
            wrapper.push_string( &err );
        }
    }
    1
}

extern fn reset( _: LuaState ) -> i32 {
    let wrapper = unsafe { LUA.as_ref( ).unwrap( ) };
    wrapper.check_string( 1 );
    let dir = &wrapper.get_string( 1 );
    match git2::Repository::discover( PathBuf::from( "garrysmod/" ).join( PathBuf::from( dir ) ) ) {
        Ok( repo ) => {
            /* fetch */
            let mut remote = repo.find_remote( "origin" ).unwrap( );
            remote.fetch( &[ "master" ], None, None ).unwrap( );

            /* reset */
            let oid = repo.refname_to_id( "refs/remotes/origin/master" ).unwrap( );
            let object = repo.find_object( oid, None ).unwrap();
            repo.reset( &object, git2::ResetType::Hard, None ).unwrap( );
            wrapper.push_string( "successfully reseted repository" );
        },
        Err( _ ) => {
            let dir = String::from( dir );
            let err = "failed to discover repository \n".to_owned( ) + &dir;
            wrapper.push_string( &err );
        }
    }
    1
}

#[no_mangle]
pub extern fn gmod13_open( state: LuaState ) -> i32 {
    unsafe { LUA = Some( GLuaWrapper { state: state } ); }
    let wrapper = unsafe { LUA.as_ref( ).unwrap( ) };
    wrapper.push_global( );
    wrapper.create_table( );
    wrapper.push_cfunction( clone ); wrapper.set_field( -2, "clone" );
    wrapper.push_cfunction( fetch ); wrapper.set_field( -2, "fetch" );
    wrapper.push_cfunction( reset ); wrapper.set_field( -2, "reset" );
    wrapper.set_field( -2, "git" );
    0
}

#[no_mangle]
pub extern fn gmod13_close( _: LuaState ) -> i32 { 0 }
